#pragma once

class Scheme
{
public:

    virtual int operator () (const int i) const = 0;

    template <typename T>
    std::function<int(int)>
    operator * (T func)
    {
        return
            std::function<int(int)>
            (
                [=] (const int i)
                {
                    return (*this)(i) * func(i);
                }
            );
    }

    template <typename T>
    std::function<int(int)>
    operator + (T func)
    {
        return
            std::function<int(int)>
            (
                [=] (const int i)
                {
                    return (*this)(i) + func(i);
                }
            );
    }
};

class FunI : public Scheme
{
public:
    int operator () (const int i) const { return i; }

};

class FunSquare : public Scheme 
{
public:
    int operator () (const int i) const { return i * i; }

};

class FunValue : public Scheme 
{
public:
    FunValue( const int n ) : n(n) {}
    int operator () (const int i) const { return n; }
private:
    const int n;
};

template <typename Func1, typename Func2>
std::function<int(int)>
funMul(Func1 func1, Func2 func2)
{
    return { [&] (const int i) { return func1(i) * func2(i); } };
}
