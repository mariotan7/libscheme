#include <iostream>
#include <cstdlib>
#include <functional>

template <typename Func>
int for_each
(
    const int n,
    const Func & func
){
    int x = 0;
    for ( int i = 1 ; i <= n ; i++ ) {
        x += func(i);
    }

    return x;
}

template <typename F1, typename F2>
class FunctorAdd;

template <typename F1, typename F2>
class FunctorMul;

class Functor
{
public:

    virtual int operator () (const int i) const = 0;

    template <typename F2>
    FunctorMul<Functor,F2>
    operator * (F2 func) const
    {
        return FunctorMul<Functor, F2>( *this, func );
    }

    template <typename F2>
    FunctorAdd<Functor,F2>
    operator + (F2 func) const
    {
        return FunctorAdd<Functor, F2>( *this, func );
    }

};

template <typename F1, typename F2>
class FunctorAdd : public Functor
{
public:
    FunctorAdd( const F1 & func1, const F2 & func2 )
        : func1( func1 ), func2( func2 ){}

    int operator () (const int i) const
    {
        return func1(i) + func2(i);
    }

private:
    const F1 & func1;
    const F2 & func2;
};

template <typename F1, typename F2>
class FunctorMul : public Functor
{
public:
    FunctorMul( const F1 & func1, const F2 & func2 )
        : func1( func1 ), func2( func2 ){}

    int operator () (const int i) const
    {
        return func1(i) * func2(i);
    }

private:
    const F1 & func1;
    const F2 & func2;
};


class LHSArray
{
public:
    LHSArray( int * const array )
        : array(array) {}

    SubArray operator = (const Functor & func)
    {
        return Subarray( array, rhs );
    }

private:
    double * const array;
};

class SubstituteArray
{
public:
    SubAry( int * const array, const & Functor rhs )
        : array(array) rhs(rhs) {}

    void
    operator () (const int i)
    {
        array[i] = rhs(i);
    }

private:
    const Functor & rhs;
};


class FunI : public Functor
{
public:
    int operator () (const int i) const { return i; }

};

class FunSquare : public Functor
{
public:
    int operator () (const int i) const { return i * i; }

};

class FunValue : public Functor
{
public:
    FunValue( const int n ) : n(n) {}
    int operator () (const int i) const { return n; }
private:
    const int n;
};



int main( int argc, char * argv[] )
{
    if ( argc != 2 ) {
        std::cerr << "err." << std::endl;
        std::cerr << argv[0] << " <n>" << std::endl;
        return 1;
    }

    const int n = std::atoi(argv[1]);
    const int value = 10;

    //const int y = for_each( n, funMul(FunValue(value), FunI()) );
    const int z = for_each( n, FunValue(value)*FunI() );
    const int w = for_each( n, FunValue(value)+FunI() );
    const int h = for_each( n, FunValue(value)*FunValue(value)+FunI() );

    int zz = 0;
    int ww = 0;
    int hh = 0;
    for ( int i = 1 ; i <= n ; i++ ) {
        zz += value * i;
        ww += value + i;
        hh += value*value + i;
    }

    //std::cout << "x = " << x << "\t" << xx << std::endl;
    //std::cout << "y = " << y << "\t" << yy << std::endl;
    std::cout << "w = " << w << "\t" << ww << std::endl;
    std::cout << "z = " << z << "\t" << zz << std::endl;
    std::cout << "h = " << h << "\t" << hh << std::endl;
}
